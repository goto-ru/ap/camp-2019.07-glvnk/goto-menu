"""site URL Configuration"""
from django.contrib import admin
from django.urls import path

from rsre.views import menuoo

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', menuoo),
]
