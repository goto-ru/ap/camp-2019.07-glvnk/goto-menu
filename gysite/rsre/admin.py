from django.contrib import admin

from .models import Menu, Table

admin.site.register(Menu)
admin.site.register(Table)
