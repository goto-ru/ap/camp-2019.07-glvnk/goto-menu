# Generated by Django 2.2.3 on 2019-07-24 14:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rsre', '0003_table'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='table',
            name='parent',
        ),
        migrations.AddField(
            model_name='table',
            name='choice',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='rsre.Menu'),
            preserve_default=False,
        ),
    ]
