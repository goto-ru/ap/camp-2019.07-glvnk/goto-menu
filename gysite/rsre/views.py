import random

from django.http import HttpResponse
from django.shortcuts import render, redirect

from .models import Menu, Table


def menuoo(request):
    menus = Menu.objects.filter(parent=None)
    if menus.count() == 0:
        return HttpResponse("Ничего нет.")
    if "id" not in request.GET:
        menu = random.choice(menus)
    else:
        menu = Menu.objects.get(pk=request.GET['id'])
        menus = Menu.objects.filter(parent=menu)
    clicks = Table.objects.filter(choice=menu)
    return render(request, 'template01.html', {'menu': menu, 'menus': menus, 'clicks': clicks})
