from django.db import models


class Menu(models.Model):
    name = models.TextField(max_length=30)
    parent = models.ForeignKey('Menu', on_delete=models.CASCADE,  blank=True, null=True)

    def __str__(self):
        return self.name


class Table(models.Model):
    telegram_id = models.TextField(max_length=30)
    choice = models.ForeignKey('Menu', on_delete=models.CASCADE)

    def __str__(self):
        return self.telegram_id
